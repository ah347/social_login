## About MyApp

-   download

-   create .env file

-   setup mysql database

-   run ..
-   composer install
-   composer dump-auto
-   php artisan key:generate
-   php artisan migrate:fresh

-   php artisan serv

## visit

-   http://localhost:8000/login
-   http://localhost:8000/register

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
